# ExamenAndroid #

Proyecto realizado como prueba para determinar el desempeño en android en un plazo menor a 24 horas

### Requisitos Previos ###

* Android Studio Arctic Fox 2020.3.1 Patch 2 o superior.
* OpenJDK 17 64 Bit o superior.
* Git 2.x o superior
* [Acceso al repositorio](https://bitbucket.org/kennedress/examenandroid2022/src/master/).

### Instalación ###

* Abrir el proyecto en Android Studio.
* Conectar un dispositivo que cumpla con los requisitos para su correcto uso.
* Ejecutar 'Run App'.


### Construido con ###

* [Gradle](https://gradle.org/) (7.0.2) - Herramienta de automatización de construcción de código.
* [Java](https://www.java.com/) (SE 8 [*][1]) - Lenguaje para la Java Virtual Machine.
* [kotlin](https://kotlinlang.org/) (1.6.10) - Lenguaje para la Java Virtual Machine.
* [Room](https://developer.android.com/topic/libraries/architecture/room) (2.0.0) - Object Relational Mapping de Android Architecture Components.
* [Glide](https://www.glideapps.com/) (4.12.0) - Herramienta para la carga de imágenes dentro del ciclo de vida de una actividad.
* [UploadService](https://github.com/gotev/android-upload-service) (4.6.0) - Herramienta central para el envío de archivos.
* [Gson](https://mvnrepository.com/artifact/com.squareup.retrofit2/converter-gson) (2.9.0) - Herramienta para convertir objetos json.

### Versionamiento ###
* El cambio de versiones se sujeta de acuerdo a los líderes / arquitectura / mercadotécnia.
* Version 1.0

### Desarrolladores y módulos trabajados ###
* **Kenneth** - kenenthbrioneslopez@gmail.com

## Licencia ##
* Este proyecto es público.

## Estilos de codificación
* Para su siguiente versión se implementará el patrón de diseño MVVM y arquitecura limpia.

## Accesos, dudas y quejas ##
* Comunicarse con:
  **Kenneth Briones**
  kenenthbrioneslopez@gmail.com
  8121125599
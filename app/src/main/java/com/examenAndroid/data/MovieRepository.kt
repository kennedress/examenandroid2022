package com.examenAndroid.data

import com.examenAndroid.domain.model.ListMovies

/**
 * Repositorio destinado para la comunicación con la API rest.
 */
class MovieRepository {

    private val api = MovieService()

    /**
     * getAllMovies() regresa un objeto con detalles de la creación de la lista
     * así como el listado en sí de películas.
     * @return [ListMovies]? Objeto concentrador puede ser nulo, en caso de error
     */
    suspend fun getAllMovies(): ListMovies? {
        return api.getMovies()
    }
}
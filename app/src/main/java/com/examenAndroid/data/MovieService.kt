package com.examenAndroid.data

import com.examenAndroid.Constants
import com.examenAndroid.domain.model.ListMovies
import com.examenAndroid.framework.RetrofitHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Servicio que almacena la corrutina para devolver la respuesta.
 */
class MovieService {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getMovies(): ListMovies? {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(RetrofitMovieClient::class.java).getMovieList(Constants.apiKeyREST)
            response.body()
        }
    }
}
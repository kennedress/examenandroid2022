package com.examenAndroid.data

import com.examenAndroid.domain.model.ListMovies
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Representa el consumo de api.
 */
interface RetrofitMovieClient {
    /**
     * De manera predeterminada estamos en la sección 3 del listado 5 para agilizar el desarrollo
     * @param apiKey [String] Llave de acceso solicitada para themoviedb.org
     */
    @GET("/3/list/5")
    suspend fun getMovieList(@Query("api_key") apiKey: String): Response<ListMovies>
}
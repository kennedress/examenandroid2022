package com.examenAndroid

import android.app.Application
import androidx.room.Room
import com.examenAndroid.framework.room.ListMovieDatabase

/**
 * Inicializador de la aplicación.
 */
class ExamenAndroidApp: Application() {
    companion object {
        // Trae la base de datos con sus acciones correspondientes.
        lateinit var databaseListMovie: ListMovieDatabase
    }

    override fun onCreate() {
        super.onCreate()
        databaseListMovie = Room.databaseBuilder(this, ListMovieDatabase::class.java, "movies-db").allowMainThreadQueries().build()
    }
}
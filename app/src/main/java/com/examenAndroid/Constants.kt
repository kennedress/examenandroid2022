package com.examenAndroid

/**
 * Guarda la información que no cambiará
 */
object Constants {
    /**
     * Esto es inseguro, mandar a un json para procesarlo de lado de BuildConfig
     * dentro del gradle
     */
    const val namePosition = "positions"
    const val apiKeyREST = "cf0cda709b779b6fc59c358f30d50f2a"
    const val apiUrl = "https://api.themoviedb.org"
}
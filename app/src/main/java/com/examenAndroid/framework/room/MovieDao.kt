package com.examenAndroid.framework.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.examenAndroid.domain.model.Movie

/**
 * Acciones a realizar con la tabla de objeto de películas.
 */
@Dao
interface MovieDao {
    // Busca todas las películas almacenadas
    @Query("SELECT * FROM movie")
    fun getAllMovie(): List<Movie>

    // Inserta una película
    @Insert
    fun insertDetailListMovie(vararg movie: Movie)

    // Elimina una película
    @Query("DELETE FROM movie")
    fun deleteDetailListMovie()
}
package com.examenAndroid.framework.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * Acciones a realizar con la tabla de objeto de películas.
 */
@Dao
interface ListMovieDao {
    // Obtiene el detalle del listado
    @Query("SELECT * FROM list_movies")
    fun getDetailListMovie(): ListMoviesRoom

    // Inserta el detalle del listado
    @Insert
    fun insertDetailListMovie(vararg listMovie: ListMoviesRoom)

    // Elimina el detalle del listado anterior
    @Query("DELETE FROM list_movies")
    fun deleteDetailListMovie()
}
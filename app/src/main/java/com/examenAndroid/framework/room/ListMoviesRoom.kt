package com.examenAndroid.framework.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Representa el detalle del listado de películas
 * Se optó por separarlo ya que se tiene un objeto dentro de otro objeto en el resultaado
 * del consumo de la api, apenas usé por primera vez Room, buscaré como optimizarlo.
 */
@Entity(tableName = "list_movies")
data class ListMoviesRoom(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "created_by") val createdBy: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "favorite_count") val favoriteCount: Int,
    @ColumnInfo(name = "item_count") val itemCount: Int,
    @ColumnInfo(name = "iso_639_1") val iso_639_1: String,
    @ColumnInfo(name = "poster_path") val posterPath: String,
)
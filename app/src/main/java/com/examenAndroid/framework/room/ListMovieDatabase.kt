package com.examenAndroid.framework.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.examenAndroid.domain.model.Movie

/**
 * Representa el acceso a las acciones de cada tabla.
 */
@Database(entities = [ListMoviesRoom::class, Movie::class], version = 1)
abstract class ListMovieDatabase : RoomDatabase() {
    abstract fun listMovieDao(): ListMovieDao

    abstract fun movieDao(): MovieDao
}
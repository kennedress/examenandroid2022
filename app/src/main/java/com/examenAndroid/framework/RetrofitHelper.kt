package com.examenAndroid.framework

import com.examenAndroid.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Crea el objeto para el consumo de apis.
 */
object RetrofitHelper {
    /**
     * @return [Retrofit] devuelve nuestro objeto Retrofit para consumir la api.
     */
    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.apiUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}
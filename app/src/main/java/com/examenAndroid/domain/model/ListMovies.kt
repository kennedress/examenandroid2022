package com.examenAndroid.domain.model

import com.google.gson.annotations.SerializedName

/**
 * Modelo para el objeto de lista de péliculas.
 */
data class ListMovies(
    @SerializedName("id")
    val id: String,
    @SerializedName("created_by")
    val createdBy: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("favorite_count")
    val favoriteCount: Int,
    @SerializedName("item_count")
    val itemCount: Int,
    @SerializedName("iso_639_1")
    val iso_639_1: String,
    @SerializedName("poster_path")
    val posterPath: String,
    @SerializedName("items")
    var items: List<Movie>
)
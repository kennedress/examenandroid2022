package com.examenAndroid.domain.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Modelo basado para cada película
 * se crea con Room para persistencia de datos.
 */
@Entity(tableName = "movie")
class Movie(
    @SerializedName("id")
    @PrimaryKey val id: Int,
    @SerializedName("adult")
    @ColumnInfo(name = "adult") val isAdult: Boolean,
    @SerializedName("backdrop_path")
    @ColumnInfo(name = "backdrop_path") val backdropPath: String,
    @SerializedName("media_type")
    @ColumnInfo(name = "media_type") val mediaType: String,
    @SerializedName("original_language")
    @ColumnInfo(name = "original_language") val originalLanguage: String,
    @SerializedName("original_title")
    @ColumnInfo(name = "original_title") val originalTitle: String,
    @SerializedName("overview")
    @ColumnInfo(name = "overview") val overview: Boolean,
    @SerializedName("popularity")
    @ColumnInfo(name = "popularity") val popularity: Double,
    @SerializedName("poster_path")
    @ColumnInfo(name = "poster_path") val poster_path: String,
    @SerializedName("release_date")
    @ColumnInfo(name = "release_date") val releaseDate: String,
    @SerializedName("title")
    @ColumnInfo(name = "title") val title: String,
    @SerializedName("video")
    @ColumnInfo(name = "video") val video: Boolean,
    @SerializedName("vote_average")
    @ColumnInfo(name = "vote_average") val voteAverage: Float,
    @SerializedName("vote_count")
    @ColumnInfo(name = "vote_count") val voteCount: Int,
    @ColumnInfo(name = "is_expand") var isExpand: Boolean = false
)
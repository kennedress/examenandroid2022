package com.examenAndroid.domain.usecases

import com.examenAndroid.data.MovieRepository
import com.examenAndroid.domain.model.ListMovies

/**
 * Caso de uso para consultar el listado de películas.
 */
class GetListMovieUseCase {
    private val repository = MovieRepository()

    // consume el servicio a través de la corrutina
    suspend operator fun invoke(): ListMovies? = repository.getAllMovies()

}
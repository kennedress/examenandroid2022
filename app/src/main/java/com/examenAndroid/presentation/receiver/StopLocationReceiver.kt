package com.examenAndroid.presentation.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.examenAndroid.presentation.job.SettingsJob
import com.examenAndroid.presentation.notification.JobNotification

/**
 * Llama la acción para finalizar el reporte
 */
class StopLocationReceiver : BroadcastReceiver() {

    /**
     * Llama las funciones correspondientes para finalizar el reporte
     */
    override fun onReceive(context: Context, intent: Intent) {
        SettingsJob.stopLocationJob(context)
        JobNotification.stopNotifcation(context)
    }
}
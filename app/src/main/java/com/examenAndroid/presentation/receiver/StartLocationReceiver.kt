package com.examenAndroid.presentation.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import com.examenAndroid.presentation.job.SettingsJob
import com.examenAndroid.presentation.notification.JobNotification

/**
 * Llama la acción de reportar en caso de que se reinicie el dispositivo
 * Para esta acción es requerido almacenar un booleano por Room u otros medios
 * En caso de que el switch esté apagado, no debe entrar aquí
 */
class StartLocationReceiver : BroadcastReceiver() {

    /**
     * Utilizar una alternativa para versiones anteriores
     * Alarm Manager por ejemplo
     */
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onReceive(context: Context, intent: Intent) {
        // Validar primero si se activó el estar reportando si es así entonces si comenzamos
        // si no, pos no
        SettingsJob.startLocationJob(context)
        JobNotification.startNotification(context)
    }
}
package com.examenAndroid.presentation.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.examenAndroid.R
import com.examenAndroid.databinding.HomeFragmentBinding
import com.examenAndroid.presentation.job.SettingsJob
import com.examenAndroid.presentation.notification.JobNotification

/**
 * Parte inicial de la aplicación.
 */
class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var binding: HomeFragmentBinding
    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        binding.btnGoToListMovie.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_APIServiceFragment)
        }

        binding.swEnablePosition.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                // solicitamos permisos, claro primero debemos mostrar el splash informativo
            } else {
                SettingsJob.startLocationJob(binding.root.context)
                JobNotification.startNotification(binding.root.context)
            }
        }
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this)[HomeViewModel::class.java]
    }
}
package com.examenAndroid.presentation.fragments

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.examenAndroid.databinding.FragmentApiRestBinding
import com.examenAndroid.presentation.adapters.RVListMovieAd
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Representa la vista general para el consumo de apis.
 */
class APIServiceFragment : Fragment() {

    companion object {
        fun newInstance() = APIServiceFragment()
    }

    private lateinit var binding: FragmentApiRestBinding
    private lateinit var viewModel: APIServiceViewModel

    // Crea nuestra vista
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentApiRestBinding.inflate(inflater, container, false)
        binding.srlListMovies.setOnRefreshListener { callRefreshList(true) }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED){
                viewModel.listMovieState.collect { state ->
                    when (state) {
                        is ListMoviesState.Success -> {
                            binding.rvListMovies.adapter = RVListMovieAd(state.listMovie.items)
                            binding.pbListMovies.visibility = View.GONE
                        }
                        is ListMoviesState.Loading -> {

                            if (state.isLoading) {
                                binding.pbListMovies.visibility = View.VISIBLE
                                binding.srlListMovies.visibility = View.GONE
                            } else {
                                binding.pbListMovies.visibility = View.GONE
                                binding.srlListMovies.visibility = View.VISIBLE
                            }
                        }
                        is ListMoviesState.Failed -> {

                        }
                    }
                }
            }
        }
        callRefreshList(false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this)[APIServiceViewModel::class.java]
    }

    private fun callRefreshList(isSwipe: Boolean) {
        viewModel.getMovies(isSwipe)
        binding.srlListMovies.isRefreshing = false
    }
}
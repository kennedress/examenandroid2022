package com.examenAndroid.presentation.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.examenAndroid.ExamenAndroidApp
import com.examenAndroid.domain.model.ListMovies
import com.examenAndroid.domain.usecases.GetListMovieUseCase
import com.examenAndroid.framework.room.ListMoviesRoom
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

/**
 * ViewModel para el consumo de apis.
 */
class APIServiceViewModel : ViewModel() {

    private val getMovieUseCase = GetListMovieUseCase()
    private val _listMovieState = MutableStateFlow<ListMoviesState>(ListMoviesState.Loading(true))
    val listMovieState = _listMovieState

    fun getMovies(isSwipeRefresh: Boolean) {
        _listMovieState.value = ListMoviesState.Loading(true)
        if (isSwipeRefresh) {
            getMoviesRemote()
        } else {
            getMoviesLocal()
        }
    }

    private fun getMoviesRemote() {
        viewModelScope.launch {
            getMovieUseCase.invoke()?.let {
                // Eliminamos si había algo guardado localmente
                ExamenAndroidApp.databaseListMovie.listMovieDao().deleteDetailListMovie()
                ExamenAndroidApp.databaseListMovie.movieDao().deleteDetailListMovie()
                // Actualizamos el objeto local
                ExamenAndroidApp.databaseListMovie.listMovieDao().insertDetailListMovie(
                    ListMoviesRoom(
                        id = it.id.toInt(),
                        createdBy = it.createdBy,
                        name = it.name,
                        description = it.description,
                        favoriteCount = it.favoriteCount,
                        itemCount = it.itemCount,
                        iso_639_1 = it.iso_639_1,
                        posterPath = it.posterPath
                    )
                )
                it.items.forEach { movie ->
                    ExamenAndroidApp.databaseListMovie.movieDao().insertDetailListMovie(movie)
                }
                // Actualizamos la vista
                _listMovieState.value = ListMoviesState.Success(it)
            } ?: kotlin.run {
                _listMovieState.value = ListMoviesState.Failed("Error")
            }
        }
    }

    private fun getMoviesLocal() {
        // Consultamos los datos de manera local, si están no vamos al remoto
        val detailListMovie = ExamenAndroidApp.databaseListMovie.listMovieDao().getDetailListMovie()
        val listMovie = ExamenAndroidApp.databaseListMovie.movieDao().getAllMovie()

        if (listMovie.isNotEmpty()) {
            _listMovieState.value =
                ListMoviesState.Success(
                    ListMovies(
                    id = detailListMovie.id.toString(),
                    createdBy = detailListMovie.createdBy,
                    name = detailListMovie.name,
                    description = detailListMovie.description,
                    favoriteCount = detailListMovie.favoriteCount,
                    itemCount = detailListMovie.itemCount,
                    iso_639_1 = detailListMovie.iso_639_1,
                    posterPath = detailListMovie.posterPath,
                    items = listMovie
                )
                )
        } else {
            getMoviesRemote()
        }
    }
}

sealed class ListMoviesState {
    data class Success(val listMovie: ListMovies): ListMoviesState()
    data class Failed(val message: String): ListMoviesState()
    data class Loading(val isLoading: Boolean): ListMoviesState()
}
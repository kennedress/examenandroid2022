package com.examenAndroid.presentation.job

import android.Manifest
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.text.format.DateFormat
import androidx.core.app.ActivityCompat
import com.examenAndroid.Constants
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*
import kotlin.collections.HashMap

/**
 * Clase para enviar las posiciones
 */
class SendLocationJob : JobService() {
    private lateinit var locationManager: LocationManager

    override fun onStartJob(params: JobParameters?): Boolean {
        startLocationUpdates()
        SettingsJob.startLocationJob(applicationContext)
        return false
    }

    override fun onStopJob(params: JobParameters?) = false

    /**
     * Encapsula los datos del GPS del dispositivo
     * Separar a otro objeto, este modelo debería contener solo JobServices.
     */
    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2 * 20 * 1000, 10.toFloat(), locationListenerGPS)

        if (lastLocation == null) {
            lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        }

        lastLocation?.let {
            // Enviamos la ubicación
            // Refactorizar para separar esta lógica
            val position = HashMap<String, Any>()
            position["bearing"] = it.bearing.toInt()
            position["latitude"] = it.latitude
            position["longitude"] = it.longitude
            position["accuracy"] = it.accuracy.toInt()
            position["locale"] = convertDate(Date().time)

            val db = FirebaseFirestore.getInstance()
            db.collection(Constants.namePosition)
                .add(position)
                .addOnSuccessListener {

                }
                .addOnFailureListener {

                }
        }
    }

    private fun stopLocationUpdates() {
        locationManager.removeUpdates(locationListenerGPS)
    }

    private val locationListenerGPS = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            lastLocation = location
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) { }

        override fun onProviderEnabled(provider: String) { }

        override fun onProviderDisabled(provider: String) { }

    }

    private fun convertDate(dateInmilliSeconds: Long, dateFormat: String = "dd/MM/yyyy hh:mm:ss") =
        DateFormat.format(dateFormat, dateInmilliSeconds).toString()

    companion object {
        const val TAG = "SLS"
        var lastLocation: Location? = null
    }
}
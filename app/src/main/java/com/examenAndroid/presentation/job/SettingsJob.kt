package com.examenAndroid.presentation.job

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi

/**
 * Concentra la configuración por default para el consumo de job
 */
class SettingsJob {
    companion object {
        /**
         * Comienza el servicio en primer / segundo plano
         * @param context [Context] contexto de donde es llamado
         * @param minimumLatency [Long] variable para hacer la separación entre cada consulta
         */
        // Revisar para versiones anteriores
        // usar AlarmManager
        @RequiresApi(Build.VERSION_CODES.M)
        fun startLocationJob(context: Context, minimumLatency: Long = 300) {
            val serviceComponent = ComponentName(context, SendLocationJob::class.java)
            val jobBuilder = JobInfo.Builder(0, serviceComponent)
                .setRequiresDeviceIdle(false)
                .setRequiresCharging(false)
                .setPersisted(true)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)

            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> {
                    // esperamos al menos
                    jobBuilder.setMinimumLatency(minimumLatency * 1000)
                    // le damos un máximo de espera
                    jobBuilder.setTriggerContentMaxDelay((minimumLatency + 1) * 2 * 1000)
                }
                else -> {
                    // Para periodos inferiores de a 15 minutos solo funciona con versiones anteriores
                    // (probar porque no estoy seguro :S)
                    jobBuilder.setPeriodic(minimumLatency * 1000)
                    // criterio para máximo de espera
                    jobBuilder.setBackoffCriteria(minimumLatency * 1000, JobInfo.BACKOFF_POLICY_LINEAR)
                }
            }

            val jobScheduler = context.getSystemService(JobScheduler::class.java)
            jobScheduler.schedule(jobBuilder.build())
        }

        // Detiene la operación de cada reporte
        fun stopLocationJob(context: Context) {
            with (context.applicationContext.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler) {
                cancelAll()
            }
        }
    }
}
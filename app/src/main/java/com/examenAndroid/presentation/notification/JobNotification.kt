package com.examenAndroid.presentation.notification

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.examenAndroid.R
import com.examenAndroid.presentation.receiver.StopLocationReceiver

/**
 * Crea la notificación para reportar y finalizar los reportes.
 */
class JobNotification {
    companion object {
        /**
         * Estos valores deben cambiarse por aplicación
         * para que no choquen sus canales
         */
        private const val notificationId = 1
        private const val CHANNEL_ID = "CHANNEL_ID"
        private const val CHANNEL_NAME = "CHANNEL_NAME"
        private const val CHANNEL_DESCRIPTON = "CHANNEL_DESCRIPTON"

        /**
         * Inicia la notificación
         * @param context[Context] contexto de donde es llamado
         */
        fun startNotification(context: Context) {
            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_job_notification_icon_24)
                .setContentTitle("título")
                .setContentText("Contenido")
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .addAction(R.drawable.ic_baseline_close_24, context.getString(R.string.btn_stop_service), createAction(context))

            createNotificationChannerl(context, importance = NotificationCompat.PRIORITY_HIGH)
            with (NotificationManagerCompat.from(context)) {
                // notificationId es un único valor entero para cada tipo de notificación por definir
                notify(notificationId, builder.build())
            }
        }

        /**
         * Finaliza las notificaciones
         * @param context[Context] contexto de donde es llamado
         */
        fun stopNotifcation(context: Context) {
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            // quitamos las notificaciones generales chingue su madre
            // si quieres quitar una usa cancel(notificaionId)
            notificationManager.cancelAll()
        }

        // Creamos los botones de la notificación
        // para versiones iguales o superiores a android 12 debemos levantar una bandera de cambio
        // si no, truena
        @SuppressLint("UnspecifiedImmutableFlag")
        private fun createAction(context: Context): PendingIntent {
            val actionIntent = Intent(context, StopLocationReceiver::class.java)
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent.getBroadcast(context, 0, actionIntent, PendingIntent.FLAG_MUTABLE)
            } else {
                PendingIntent.getBroadcast(context, 0, actionIntent, PendingIntent.FLAG_ONE_SHOT)
            }
        }

        // Creamos el canal correspondiente, únicamente para versiones iguales o superiores a android 8
        private fun createNotificationChannerl(context: Context, importance: Int = NotificationCompat.PRIORITY_DEFAULT) {
            /**
             * Creamos una canal de Notificación pero solo para versiones iguales o superiores al API 26
             * ya que la clase NotificacionChannel es nueva y no tiene soporte para versiones anteriores
             */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val importanceChannel = when (importance) {
                    NotificationCompat.PRIORITY_HIGH -> NotificationManager.IMPORTANCE_HIGH
                    else -> NotificationManager.IMPORTANCE_DEFAULT
                }

                val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importanceChannel).apply {
                    description = CHANNEL_DESCRIPTON
                }
                // Registramos el canal con el sistema de servicio
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }
    }
}
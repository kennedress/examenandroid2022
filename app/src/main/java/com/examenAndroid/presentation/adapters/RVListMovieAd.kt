package com.examenAndroid.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.examenAndroid.R
import com.examenAndroid.databinding.ItemMovieBinding
import com.examenAndroid.domain.model.Movie

/**
 * Representa el listado de películas.
 * @param items[List<Movie>] el listado concentrado de películas
 */
class RVListMovieAd(
    private val items: List<Movie>
) : RecyclerView.Adapter<RVListMovieAd.MovieVH>() {

    // Determina la vista a utilizar, puede filtrarse para mostrar diferentes vistas según
    // el contenido de cada objeto.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieVH {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMovieBinding.inflate(inflater, parent, false)
        return MovieVH(binding)
    }

    // Manda los datos a cada vista
    override fun onBindViewHolder(holder: MovieVH, position: Int) {
        holder.bind(items[position])
    }

    // Muestra el número de elementos que se mostrarán en nuestra lista
    override fun getItemCount(): Int = items.count()

    /**
     * Concentra la información para pasar a cada item de la lista
     * @param binding[ItemMovieBinding] representa nuestra vista
     */
    class MovieVH(private val binding: ItemMovieBinding)
        : RecyclerView.ViewHolder(binding.root) {

        // Asigna los datos a nuestra vista
        fun bind(item: Movie) {
            binding.run {
                tvTitle.text = item.title
                tvVoteAverage.text = root.context.getString(R.string.tv_vote_average, item.voteAverage)

                clItem.setOnClickListener { item.isExpand = !item.isExpand }
                clContent.visibility =
                    if (item.isExpand) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
            }
        }
    }
}